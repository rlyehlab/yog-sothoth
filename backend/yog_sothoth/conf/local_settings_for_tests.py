"""Local settings file for tests."""
# flake8: noqa

DEBUG = True
DISABLE_HTTPS_REDIRECT = True

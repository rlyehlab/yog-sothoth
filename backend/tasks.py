"""Common tasks for Invoke."""
from invoke import task

# Set the application name
APP_NAME: str = 'yog_sothoth'


@task(
    default=True,
    help={
        'development': 'run a development server'
    }
)
def runserver(ctx, development=False):
    """Run a uvicorn development server."""
    if development:
        ctx.run(
            'uvicorn yog_sothoth.app:app --reload',
            echo=True,
            pty=True,
            env={
                'YOG_DEVELOPMENT_MODE': 'true',
                'YOG_REDIS_HOST': '127.0.0.1',
                'YOG_ALLOWED_HOSTS': '127.0.0.1',
                'YOG_EMAIL_HOST': '127.0.0.1',
                'YOG_EMAIL_PORT': '8025',
                'YOG_EMAIL_SENDER_ADDRESS': 'yog_sothoth@localhost',
                'YOG_MANAGERS_ADDRESSES': 'yog_managers@localhost',
                'YOG_MATRIX_URL': 'http://127.0.0.1:8000/v1/matrix',
                'YOG_MATRIX_REGISTRATION_SHARED_SECRET': 'fakesecret',
            }
        )
    else:
        ctx.run('gunicorn --config yog_sothoth/conf/gunicorn.py yog_sothoth.app:app',
                echo=True)


@task
def flake8(ctx):
    """Run flake8 with proper exclusions."""
    ctx.run(f'flake8 --exclude migrations,local_settings.py {APP_NAME}/', echo=True)


@task
def pydocstyle(ctx):
    """Run pydocstyle with proper exclusions."""
    cmd = f'find {APP_NAME}/'
    ctx.run(
        cmd + ' -type f \\( -path "*/migrations/*" -o -path "*/local_settings.py" \\) '
              '-prune -o -name "*.py" -exec pydocstyle --explain "{}" \\+',
        echo=True,
    )


@task
def bandit(ctx):
    """Run bandit with proper exclusions."""
    ctx.run(f'bandit -i -r --exclude local_settings.py {APP_NAME}/', echo=True)


@task
def lint_docker(ctx):
    """Lint Dockerfile."""
    ctx.run('sudo docker run --rm -i hadolint/hadolint < Dockerfile', echo=True,
            pty=True, echo_stdin=False)


# noinspection PyUnusedLocal
@task(flake8, pydocstyle, bandit, lint_docker)
def lint(ctx):
    """Lint code and static analysis."""


@task
def redis(ctx):
    """Run a temporal Redis docker container."""
    ctx.run('sudo docker run --rm --network host redis:5-alpine', echo=True, pty=True,
            echo_stdin=False)


@task
def aiosmtpd(ctx):
    """Run a temporal email server for development."""
    ctx.run('python3 -m aiosmtpd -n', echo=True, pty=True,
            echo_stdin=False)


@task
def build(ctx, tag='latest', build_for_tests=False):
    """Build Yog-Sothoth API Docker image."""
    build_arg = '--build-arg BUILD_FOR_TESTS=1 ' if build_for_tests else ''
    ctx.run(f'sudo docker build --compress --pull --rm {build_arg}'
            f'--tag registry.rlab.be/sysadmins/yog_sothoth:{tag} .', echo=True,
            pty=True, echo_stdin=False)


@task
def clean(ctx):
    """Remove all temporary and compiled files."""
    remove = (
        'build',
        'dist',
        '*.egg-info',
        '.coverage',
        'cover',
        'htmlcov',
    )
    ctx.run(f'rm -vrf {" ".join(remove)}', echo=True)
    ctx.run('find . -type d -name "__pycache__" -exec rm -rf "{}" \\+', echo=True)
    ctx.run('find . -type f -name "*.pyc" -delete', echo=True)
